# -*- mode: python ; coding: utf-8 -*-


a = Analysis(
    ['Windows壁纸设置程序.py'],
    pathex=[],
    binaries=[],
    datas=[('壁纸', '壁纸'), ('图标/favicon.ico', '图标')],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    noarchive=False,
    optimize=0,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.datas,
    [],
    name='Windows壁纸设置程序 - 黄朝勋',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=False,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon=['图标\\favicon.ico'],
)
