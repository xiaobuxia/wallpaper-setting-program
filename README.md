# Windows壁纸设置程序

#####  date：2024.8.15
#####  update time: 2024.8.15
#####  name： Windows壁纸设置程序
#####  make：黄朝勋

#### 介绍
##### —V1.0：使用python编写的Windows壁纸设置程序，实现1分钟自动切换壁纸，密码防退出功能，打包成单程序后更适合企业推送在企业所有计算机实现一键切换所有计算机壁纸，实现企业文化宣传。
![输入图片说明](https://foruda.gitee.com/images/1723684190171801151/17503062_5124065.jpeg "软件截图.jpg")


#### 软件架构
软件使用python编写，支持所有windows操作系统，python版本建议不低于3.8.5，IDE编辑器推荐：pycharm，依赖库包含（自行安装）：

```
import os
import sys
import time
import ctypes
import tkinter as tk
from tkinter import messagebox
import subprocess
import threading
from PIL import Image, ImageDraw
import pystray
from filelock import FileLock, Timeout
```

#### 使用说明

1.  安装依赖库：pip install，壁纸文件夹为壁纸存放位置，请自行准备壁纸，壁纸文件夹可以放多张壁纸，实现1分钟切换，壁纸尺寸建议： **6827X3840** 
2.  打包命令：pyinstaller -F -w --add-data "壁纸;壁纸" --add-data "图标/favicon.ico;图标" --icon=图标/favicon.ico --name="Windows壁纸设置程序 - 黄朝勋" Windows壁纸设置程序.py
3.  使用程序即可

#### 效果图
![输入图片说明](%E5%9B%BE%E6%A0%87/2024-08-15%20091714.jpg)

## 联系方式：若有实际需求及安装问题，请加微信了解！
![输入图片说明](%E5%9B%BE%E6%A0%87/15.jpg)
## 欢迎你的赞助，你的赞助是我前进的动力
![输入图片说明](%E5%9B%BE%E6%A0%87/16.jpg)


